/*Файл содержит вещественные числа. Вычислить сумму всех локальных минимумов в
файле. Локальный минимум – значение меньшее предыдущего и последующего значений
в файле.*/


#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE *file;
    char str[80];
    double summa=0,num1=0,num2=0,num3=0;
    file=fopen("Num","r");
    if(file!=NULL){
        fgets(str,80,file);
        printf("Числа из файла : ");
        puts(str);
        fclose(file);
        file=fopen("Num","r");
        fscanf(file,"%lf %lf %lf",&num1,&num2,&num3);
        while(!feof(file)){
            if(num2 < num1 && num2 < num3)
                summa+=num2;
            fscanf(file,"%lf %lf %lf",&num1,&num2,&num3);
        }
        fclose(file);
        printf("Сумма = %lf ",summa);
    }
    else
        printf("Ошибка открытия файла");

    return 0;

}
